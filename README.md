# Analytické úkoly:

- zaměstnanost např. v technologických společnostech (nabírání kvůli vytížení služeb, propouštění koncem pandemie kvůli snížení vytížení)
- souvislost mezi počtem nakažených/mrtvých/očkovaných ve světě
- souvislost věku/pohlaví/onemocnění s úmrtím
- počet českých domácností s internetem/počítačem (očekávaný vyšší nárust převážně u nízkopříjmových rodin)
- 

# Backlog

## 27.02.

### Kasík

- založení projektu, nástřel úkolů
